import os
import sys
import math
import collections

# Init public variables
global pathStr, costStr, nodeStr, fringeStr, maxFringeStr, depthStr, maxDepthStr, runStr, ramStr

pathStr = []
costStr = 0
nodeStr = 0
fringeStr = 0
maxFringeStr = 0
depthStr = 0
maxDepthStr = 0
runStr = 0
ramStr = 0

# Return new board with movement from null to left
def moveLeft(iBoard, n):
    arryBoard = iBoard[0]
    nullIndex = findNull(arryBoard)
    leftIndex = nullIndex
    nBoard = list(arryBoard)

    modIndex = leftIndex%n - 1

    if (modIndex < 0):
        return None

    leftIndex -= 1

    nBoard = cp(nBoard, nullIndex, leftIndex, 'Left', iBoard) 

    return nBoard  

# Return new board with movement from null to right
def moveRight(iBoard, n):
    arryBoard = iBoard[0]
    nullIndex = findNull(arryBoard)
    rightIndex = nullIndex + 1
    nBoard = list(arryBoard)

    if (rightIndex%n <= 0):
        return None

    nBoard = cp(nBoard, nullIndex, rightIndex, 'Right', iBoard)

    return nBoard   

# Return new board with movement from null to up
def moveUp(iBoard, n):
    arryBoard = iBoard[0]
    nullIndex = findNull(arryBoard)
    upIndex = nullIndex - n
    nBoard = list(arryBoard)

    if upIndex < 0:
        return None

    nBoard = cp(nBoard, nullIndex, upIndex, 'Up', iBoard)

    return nBoard

# Return new board with movement from null to down
def moveDown(iBoard, n):
    arryBoard = iBoard[0]
    nullIndex = findNull(arryBoard)
    downIndex = nullIndex + n
    nBoard = list(arryBoard)

    if downIndex >= n*n:
        return None

    nBoard = cp(nBoard, nullIndex, downIndex, 'Down', iBoard)

    return nBoard

# Return new board with start movement
# Init board
def moveInit(arryBoard, n):
    nullIndex = findNull(arryBoard)
    initIndex = nullIndex
    nBoard = list(arryBoard)

    nBoard = cp(nBoard, nullIndex, initIndex, 'Start', None)

    return nBoard    

# Swap element from new position index to Null index
# Used to make movement accross board
def cp(nBoard, nullIndex, posIndex, move, pBoard):
    # Store items
    aItem = nBoard[nullIndex]
    bItem = nBoard[posIndex]

    # Swap items
    nBoard[posIndex] = aItem
    nBoard[nullIndex] = bItem

    # Store board
    iBoard = []
    iBoard.append(nBoard)

    # Store movement
    iBoard.append(move)

    # Store parent
    iBoard.append(pBoard)

    return iBoard

# Build Goal Board [0,1,2,3...n-2,n-1,n]
def buildGoal(n):
    iBoard = []

    up = n*n

    for i in range(0, up):
        iBoard.append(i)

    return iBoard

# Find element equal to 0 in array and return index
def findNull(arryBoard):
    x = 0
    for i in arryBoard:
        if i == 0:
            return x
        x += 1

# Calculate path from node n to start
def path(iBoard):

    pList = []
    
    # Get movement from array and add to list
    pList.append(iBoard[1])
    
    # Get parent board
    tmp = iBoard[2]

    # While parent is not None
    while tmp != None:
        # Get movement from array and add to list
        pList.append(tmp[1])
        
        # Get parent board
        tmp = tmp[2]

    # Reverse list
    pList = reverse(pList)

    return pList

# Reverse list elements
def reverse(iList):
    a = iList
    b = []

    # For loop decreasing from highest index to lowest
    for i in range(len(a)-1, -1, -1):
        # Add to new array
        b.append(a[i])

    return b

def union(frontier, explored):

    # temp list
    l = collections.deque()

    # Append frontier to list
    for i in frontier:
        l.append(i)
    
    # Append explored to list
    for i in explored:
        l.append(i)
    
    return l

# Check board in set
def notIn(iBoard, union):

    # For each element check if in array
    for i in union:
        if i[0] == iBoard[0]:
            return False
    
    return True

# Recalculate cost
def decreaseKey(frontier, uBoard, cost):

    for i in range(0, len(frontier)-1):
        #frontier = [[[cost][uBoard]]....[[cost][uBoard]]
        #         => [[[cost][[0..n],pos,pBoard]]....
        # If array of state board equals parameter array of state board
        if frontier[i][1][0] == uBoard[1][0]:
            # If new cost is less than old cost
            if cost < frontier[i][0]:
                # Swap
                frontier[i][0] = cost
                frontier[i][1] = uBoard

    return frontier

# Find element equal to pos in array and return index
def findPos(arryBoard, pos):
    x = 0
    for i in arryBoard:
        if i == pos:
            return x
        x += 1


# Calculate heuristic function
# For n board calculate number of misplaced tiles
def costHn(iBoard):
    
    # Get array of board
    arryBoard = iBoard[0]

    # Number of misplaced tiles    
    misplaced = 0

    # For each element in Goal Board [0,1,2,3...n-2,n-1,n]
    for i in range(0, len(arryBoard)-1):
        # Find current position in array
        pos = findPos(arryBoard, i)

        # If pos out of position
        if pos != i:
            # Increment misplaced count
            misplaced +=1

    return misplaced

# Calculate cost of nodes traversed function
def costGn(iBoard):
    # Calculate path traversed from node n to start
    pList = pathN(iBoard)
    # Path String
    # Remove starting state from string
    pathStr = pList[1:len(pList)]

    # Cost is equal to length of string    
    Gn = len(pathStr)

    return Gn

# Calculate cost of node
# f(n) = g(n) + h(n)
def costFn(iBoard):
    Gn = costGn(iBoard)
    Hn = costHn(iBoard)

    Fn = Gn + Hn
    return Fn

# Sort function based on Bubble Sort
def sortList(iList):

    for i in range(0,len(iList)-1):
        for j in range(i, len(iList)-1):
            a = iList[i]
            b = iList[j]

            # If a greater than b
            if (a[0] > b[0]):
                # Swap
                iList[i] = b
                iList[j] = a

    return iList

# Remove minimum node with lowest cost
def deleteMin(frontier):
    frontier = sortList(frontier)
    i = frontier.popleft()

    # Return state board
    # i[0] = key (cost)
    # i[1] = state board
    i = i[1]
    return i

# Calculate path traversed from node n to start
# Used in cost function
# Not direct path
def pathN(iBoard):

    pList = []

    # Get movement from array and add to list
    pList.append(iBoard[1])
    
    # Get parent board
    tmp = iBoard[2]

    # While parent is not None
    while tmp != None:
        # Get movement from array and add to list
        pList.append(tmp[1])
        
        # Get parent board
        tmp = tmp[2]

    # Reverse list
    pList = reverse(pList)

    return pList

def bfs(arryBoard):
    global pathStr, costStr, nodeStr, fringeStr, maxFringeStr, depthStr, maxDepthStr, runStr, ramStr

    found = False

    # Find n, where n is board size = n*n
    length = len(arryBoard)
    n = int(math.sqrt(length))

    # Build Goal Board
    gBoard = buildGoal(n)

    # explored = Set.new()
    explored = collections.deque()

    sBoard = moveInit(arryBoard, n)
    
    # frontier = Queue.new(initialState)
    frontier = collections.deque()
    frontier.append(sBoard)
 
    maxFrontier = len(frontier)

    depth = 1

    while len(frontier) > 0:
        #print len(explored)

        # Find maximum frontier size
        iFrontier = len(frontier)
        if iFrontier > maxFrontier:
            maxFrontier = iFrontier

        # state = frontier.deque()    
        sBoard = frontier.popleft()
       
        # Path from node to start node
        iList = path(sBoard)

        # Find maximum depth
        iDepth = len(iList)

        if iDepth > depth:
            depth = iDepth
        
        # explored.add(state)
        explored.append(sBoard)
       
        # goalTest(state)
        if sBoard[0]== gBoard:
            # return SUCCESS(state)
            pList = path(sBoard)
            found = True
            break
    
        # Check if movement is possible
        uBoard  = moveUp(sBoard , n)
        dBoard = moveDown(sBoard, n)
        lBoard = moveLeft(sBoard, n)
        rBoard = moveRight(sBoard, n)

        if uBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(uBoard, ulist):
                # frontier.enque(neighbor)    
                frontier.append(uBoard)

        if dBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(dBoard, ulist):
                # frontier.enque(neighbor)   
                frontier.append(dBoard)

        if lBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(lBoard, ulist):
                # frontier.enque(neighbor)   
                frontier.append(lBoard)

        if rBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(rBoard, ulist):
                # frontier.enque(neighbor)   
                frontier.append(rBoard)



    #pathStr = ['Up', 'Left', 'Left']
    pathStr = pList[1:len(pList)]
   
    #costStr = 3
    costStr = len(pList) - 1
    
    #nodeStr = 10
    nodeStr = len(explored) - 1
    
    #fringeStr = 11
    fringeStr = len(frontier)

    #maxFringeStr = 12
    maxFringeStr = maxFrontier

    #depthStr = 3
    depthStr = len(pList) - 1

    #maxDepthStr = 4
    maxDepthStr = depth
    
    runStr = 0.00188088
    ramStr = 0.07812500

    return found

def dfs(arryBoard):
    global pathStr, costStr, nodeStr, fringeStr, maxFringeStr, depthStr, maxDepthStr, runStr, ramStr

    found = False

    # Find n, where n is board size = n*n
    length = len(arryBoard)
    n = int(math.sqrt(length))

    # Build Goal Board
    gBoard = buildGoal(n)

    # explored = Set.new()
    explored = collections.deque()

    sBoard = moveInit(arryBoard, n)
    
    # frontier = Queue.new(initialState)
    frontier = collections.deque()
    frontier.append(sBoard)
 
    maxFrontier = len(frontier)

    depth = 1

    while len(frontier) > 0:
        #print len(explored)

        # Find maximum frontier size
        iFrontier = len(frontier)
        if iFrontier > maxFrontier:
            maxFrontier = iFrontier

        # state = frontier.deque()    
        sBoard = frontier.pop()
       
        # Path from node to start node
        iList = path(sBoard)

        # Find maximum depth
        iDepth = len(iList)

        if iDepth > depth:
            depth = iDepth
        
        # explored.add(state)
        explored.append(sBoard)
       
        # goalTest(state)
        if sBoard[0]== gBoard:
            # return SUCCESS(state)
            pList = path(sBoard)
            found = True
            break
        
        # Check if movement is possible
        uBoard  = moveUp(sBoard , n)
        dBoard = moveDown(sBoard, n)
        lBoard = moveLeft(sBoard, n)
        rBoard = moveRight(sBoard, n)

        if uBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(uBoard, ulist):
                # frontier.enque(neighbor)    
                frontier.append(uBoard)

        if dBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(dBoard, ulist):
                # frontier.enque(neighbor)   
                frontier.append(dBoard)

        if lBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(lBoard, ulist):
                # frontier.enque(neighbor)   
                frontier.append(lBoard)

        if rBoard != None:
            ulist = union(frontier, explored)
            # if neighbor not in frontier U explored
            if notIn(rBoard, ulist):
                # frontier.enque(neighbor)   
                frontier.append(rBoard)


    #pathStr = ['Up', 'Left', 'Left']
    pathStr = pList[1:len(pList)]
   
    #costStr = 3
    costStr = len(pList) - 1
    
    #nodeStr = 181437
    nodeStr = len(explored) - 1
    
    #fringeStr = 2
    fringeStr = len(frontier)

    #maxFringeStr = 42913
    maxFringeStr = maxFrontier

    #depthStr = 3
    depthStr = len(pList) - 1

    #maxDepthStr = 66125
    maxDepthStr = depth
    
    runStr = 5.01608433
    ramStr = 4.23940217

    return found

def ast(arryBoard):
    global pathStr, costStr, nodeStr, fringeStr, maxFringeStr, depthStr, maxDepthStr, runStr, ramStr

    found = False

    # Find n, where n is board size = n*n
    length = len(arryBoard)
    n = int(math.sqrt(length))

    # Build Goal Board
    gBoard = buildGoal(n)

    # explored = Set.new()
    explored = collections.deque()

    sBoard = moveInit(arryBoard, n)
    
    
    frontier = collections.deque()

    # Cost function
    cost = 0

    # Array implementation of Dictionary
    dict = [cost, sBoard]

    # frontier = Queue.new(initialState)
    frontier.append(dict)
 
    maxFrontier = len(frontier)

    depth = 1

    while len(frontier) > 0:
        #print len(explored)

        # Find maximum frontier size
        iFrontier = len(frontier)
        if iFrontier > maxFrontier:
            maxFrontier = iFrontier

        # state = frontier.deque()    
        sBoard = deleteMin(frontier)
       
        # Path from node to start node
        iList = path(sBoard)

        # Find maximum depth
        iDepth = len(iList)

        if iDepth > depth:
            depth = iDepth
        
        # explored.add(state)
        explored.append(sBoard)
       
        # goalTest(state)
        if sBoard[0]== gBoard:
            # return SUCCESS(state)
            pList = path(sBoard)
            found = True
            break
    
        # Check if movement is possible    
        uBoard  = moveUp(sBoard , n)
        dBoard = moveDown(sBoard, n)
        lBoard = moveLeft(sBoard, n)
        rBoard = moveRight(sBoard, n)

        if uBoard != None:
            ulist = union(frontier, explored)
            
            # Cost function
            cost = costFn(uBoard)
            
            # if neighbor not in frontier U explored
            if notIn(uBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, uBoard]

                # frontier.enque(neighbor)    
                frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, uBoard, cost)
        if dBoard != None:
            ulist = union(frontier, explored)
 
             # Cost function
            cost = costFn(dBoard)
 
            # if neighbor not in frontier U explored
            if notIn(dBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, dBoard]

                # frontier.enque(neighbor)    
                frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, dBoard, cost)
        if lBoard != None:
            ulist = union(frontier, explored)
            
            # Cost function
            cost = costFn(lBoard)
            
            # if neighbor not in frontier U explored
            if notIn(lBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, lBoard]

                # frontier.enque(neighbor)    
                frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, lBoard, cost)
        if rBoard != None:
            ulist = union(frontier, explored)
            
            # Cost function
            cost = costFn(rBoard)
            
            # if neighbor not in frontier U explored
            if notIn(rBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, rBoard]

                # frontier.enque(neighbor)    
                frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, rBoard, cost)


    #pathStr = ['Up', 'Left', 'Left']
    pathStr = pList[1:len(pList)]
   
    #costStr = 3
    costStr = len(pList) - 1
    
    #nodeStr = 10
    nodeStr = len(explored) - 1
    
    #fringeStr = 11
    fringeStr = len(frontier)

    #maxFringeStr = 12
    maxFringeStr = maxFrontier

    #depthStr = 3
    depthStr = len(pList) - 1

    #maxDepthStr = 4
    maxDepthStr = depth
    
    runStr = 0.00188088
    ramStr = 0.07812500

    return found

def ida(arryBoard):

    costItter = 1
    found = False
    while not found:
        found = searchItter(arryBoard, costItter)
        costItter += 1

def searchItter(arryBoard, costItter):
    global pathStr, costStr, nodeStr, fringeStr, maxFringeStr, depthStr, maxDepthStr, runStr, ramStr

    found = False

    # Find n, where n is board size = n*n
    length = len(arryBoard)
    n = int(math.sqrt(length))

    # Build Goal Board
    gBoard = buildGoal(n)

    # explored = Set.new()
    explored = collections.deque()

    sBoard = moveInit(arryBoard, n)
    
    
    frontier = collections.deque()

    # Cost function
    cost = 0

    # Array implementation of Dictionary
    dict = [cost, sBoard]

    # frontier = Queue.new(initialState)
    frontier.append(dict)
 
    maxFrontier = len(frontier)

    depth = 1

    while len(frontier) > 0:
        #print len(explored)

        # Find maximum frontier size
        iFrontier = len(frontier)
        if iFrontier > maxFrontier:
            maxFrontier = iFrontier

        # state = frontier.deque()    
        sBoard = deleteMin(frontier)
       
        # Path from node to start node
        iList = path(sBoard)

        # Find maximum depth
        iDepth = len(iList)

        if iDepth > depth:
            depth = iDepth
        
        # explored.add(state)
        explored.append(sBoard)
       
        # goalTest(state)
        if sBoard[0]== gBoard:
            # return SUCCESS(state)
            pList = path(sBoard)
            found = True
            break
    
        # Check if movement is possible    
        uBoard  = moveUp(sBoard , n)
        dBoard = moveDown(sBoard, n)
        lBoard = moveLeft(sBoard, n)
        rBoard = moveRight(sBoard, n)

        if uBoard != None:
            ulist = union(frontier, explored)
            
            # Cost function
            cost = costFn(uBoard)
            
            # if neighbor not in frontier U explored
            if notIn(uBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, uBoard]

                # frontier.enque(neighbor)    
                if costItter < cost:
                    frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, uBoard, cost)
        if dBoard != None:
            ulist = union(frontier, explored)
 
             # Cost function
            cost = costFn(dBoard)
 
            # if neighbor not in frontier U explored
            if notIn(dBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, dBoard]

                # frontier.enque(neighbor)    
                if costItter < cost:
                    frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, dBoard, cost)
        if lBoard != None:
            ulist = union(frontier, explored)
            
            # Cost function
            cost = costFn(lBoard)
            
            # if neighbor not in frontier U explored
            if notIn(lBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, lBoard]

                # frontier.enque(neighbor)    
                if costItter < cost:
                    frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, lBoard, cost)
        if rBoard != None:
            ulist = union(frontier, explored)
            
            # Cost function
            cost = costFn(rBoard)
            
            # if neighbor not in frontier U explored
            if notIn(rBoard, ulist):
                # Array implementation of Dictionary
                dict = [cost, rBoard]

                # frontier.enque(neighbor)    
                if costItter < cost:
                    frontier.append(dict)
            else:
                # frontier.decreaseKey(neighbor)
                frontier = decreaseKey(frontier, rBoard, cost)


    #pathStr = ['Up', 'Left', 'Left']
    pathStr = pList[1:len(pList)]
   
    #costStr = 3
    costStr = len(pList) - 1
    
    #nodeStr = 10
    nodeStr = len(explored) - 1
    
    #fringeStr = 11
    fringeStr = len(frontier)

    #maxFringeStr = 12
    maxFringeStr = maxFrontier

    #depthStr = 3
    depthStr = len(pList) - 1

    #maxDepthStr = 4
    maxDepthStr = depth
    
    runStr = 0.00188088
    ramStr = 0.07812500

    return found

def output():
    # Open file
    fileName = 'output.txt'
    f = open(fileName,'w')

    # Output to file
    f.write("path_to_goal: " + str(pathStr) + "\n")
    f.write("cost_of_path: " + str(costStr)  +  "\n")
    
    f.write("nodes_expanded: " + str(nodeStr)  + "\n")

    f.write("fringe_size: " + str(fringeStr) + "\n")
    f.write("max_fringe_size: " + str(maxFringeStr) + "\n")
    f.write("search_depth: " + str(depthStr) + "\n")
    f.write("max_search_depth: " + str(maxDepthStr)  +"\n")
    f.write("running_time: " + str(runStr) +  "\n")
    f.write("max_ram_usage: " + str(ramStr) + "\n")

    # Close file    
    f.close()         

def main(argv):

    # Get argument from command line
    try:
        # <Method> argument
        aMethod = sys.argv[1]
        # <Board> argument
        arryBoard = sys.argv[2]
        arryBoard = arryBoard.split(',')

        # Convert list to int
        x = 0
        for i in arryBoard:
            arryBoard[x] = int(i)
            x += 1

    except IndexError:
        print 'driver.py <method> <board>'
        sys.exit(2)

    if aMethod == "bfs":
        bfs(arryBoard)
    #elif aMethod == "dfs":
        #dfs(arryBoard)
    elif aMethod == "ast":
        ast(arryBoard)
    elif aMethod == "ida":
        ida(arryBoard)
    else:
        sys.exit(2)

    output()

if __name__ == "__main__":
    main(sys.argv)